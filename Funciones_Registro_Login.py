from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common import TimeoutException
from selenium.webdriver.support.ui import Select 
from selenium.webdriver import ActionChains
from allure_commons.types import AttachmentType

from Funciones_Base import FuncionesGlobales

import allure

import pytest
import time

driver = ' '
class Funciones_Login():
    

######INICIALIZAR######
    def __init__(self,driver): #self mi o mio
        self.driver=driver
        
# Scenario 001 |Registo| ->Campos vacios
# Given que quiero probar el registo 
# When  realizo registro con capos requeridos vacios
# Then  deberia informarme que campos son requeridos sin permitir acceso

    def L1(self,menssage,menssage2,menssage3,menssage4,menssage5,menssage6,menssage7,menssage8,menssage9,menssage10,t=2): #Campos vacios requeridos
        print('###TEST  -> Campos vacios requeridos###' )    
        f=FuncionesGlobales(self.driver)
        f.Navegar('https://parabank.parasoft.com',t) #entra navegador
        self.driver.maximize_window() #maximiza
        f.Click_Mixto('xpath','//*[@id="loginPanel"]/p[2]/a',t) #hace click en registro
        f.Click_Mixto('xpath','//*[@id="customerForm"]/table/tbody/tr[13]/td[2]/input',t) #click en Register
        e1=f.SEXP('//*[@id="customer.firstName.errors"]').text  #name
        e2=f.SEXP('//*[@id="customer.lastName.errors"]').text #firt name
        e3=f.SEXP('//*[@id="customer.address.street.errors"]').text #Adrres
        e4=f.SEXP('//*[@id="customer.address.city.errors"]').text #City
        e5=f.SEXP('//*[@id="customer.address.state.errors"]').text #State
        e6=f.SEXP('//*[@id="customer.address.zipCode.errors"]').text #Zip Code 
        e7=f.SEXP('//*[@id="customer.ssn.errors"]').text #Social Security 
        e8=f.SEXP('//*[@id="customer.username.errors"]').text #Username
        e9=f.SEXP('//*[@id="customer.password.errors"]').text #Password
        e10=f.SEXP('//*[@id="repeatedPassword.errors"]').text #Password confirmation
        if (e1==menssage and e2==menssage2 and e3==menssage3 and e4==menssage4 and e5==menssage5 and e6==menssage6 and e7==menssage7 and e8==menssage8 and e9==menssage9, e10==menssage10):
            print('Prueba de Validacion de campos Requeridos fue Exitosa |No permite acceso|')
        else:
            print('Prueba de Validacion de campos Requeridos fue Incorrecta |Permite acceso')
        
        #self.driver.quit() 

# Scenario 001 |Registo| ->Campos vacios
# Given que quiero probar el registo 
# When  realizo registro con capos requeridos vacios
# Then  deberia informarme que campos son requeridos sin permitir acceso

    def L2(self,firstName,lastName,address,city,state,zipCode,ssn,username,password,repeatedPassword,menssage,t=2): #Campos requeridos
        print('###TEST -> Campos requeridos ok###')    
        f=FuncionesGlobales(self.driver)
        f.Navegar('https://parabank.parasoft.com',t) #entra al navegador
        self.driver.maximize_window() #maximiza
        f.Click_Mixto('xpath','//*[@id="loginPanel"]/p[2]/a',t) #hace click en registro
        f.Texto_Mixto('xpath','//*[@id="customer.firstName"]',firstName,t) #firstName
        f.Texto_Mixto('xpath','//*[@id="customer.lastName"]',lastName,t) #lastName
        f.Texto_Mixto('xpath','//*[@id="customer.address.street"]',address,t) #address
        f.Texto_Mixto('xpath','//*[@id="customer.address.city"]',city,t) #city
        f.Texto_Mixto('id','customer.address.state',state,t) #state
        f.Texto_Mixto('xpath','//*[@id="customer.address.zipCode"]',zipCode,t) #zipCode
        f.Texto_Mixto('xpath','//*[@id="customer.ssn"]',ssn,t) #ssn
        f.Texto_Mixto('xpath','//*[@id="customer.username"]',username,t) #username
        f.Texto_Mixto('xpath','//*[@id="customer.password"]',password,t) #password
        f.Texto_Mixto('id','repeatedPassword',repeatedPassword,t) #repeatedPassword
        f.Click_Mixto('xpath','//*[@id="customerForm"]/table/tbody/tr[13]/td[2]/input',t) #click en Register
        e1=f.SEXP('//*[@id="rightPanel"]/p').text
        if (e1==menssage):
            print('Prueba de Login Exitosa |Permite acceso ok|')
        else:
            print('Prueba de Login Incorrecta |No permite acceso|')
        #self.driver.close()
 
# Scenario 003 |Registo| ->Con usuario vigente
# Given que quiero probar el registo 
# When  realizo registro con usario ya registrado
# Then  deberia no perimitrime el ingreso        
        
    def L3(self,firstName,lastName,address,city,state,zipCode,ssn,username,password,repeatedPassword,menssage,t=2): #Campos requeridos
        print('###TEST -> Usuario ya resgitrado###')    
        f=FuncionesGlobales(self.driver)
        f.Navegar('https://parabank.parasoft.com',t) #entra al navegador
        self.driver.maximize_window() #maximiza
        f.Click_Mixto('xpath','//*[@id="loginPanel"]/p[2]/a',t) #hace click en registro
        f.Texto_Mixto('xpath','//*[@id="customer.firstName"]',firstName,t) #firstName
        f.Texto_Mixto('xpath','//*[@id="customer.lastName"]',lastName,t) #lastName
        f.Texto_Mixto('xpath','//*[@id="customer.address.street"]',address,t) #address
        f.Texto_Mixto('xpath','//*[@id="customer.address.city"]',city,t) #city
        f.Texto_Mixto('id','customer.address.state',state,t) #state
        f.Texto_Mixto('xpath','//*[@id="customer.address.zipCode"]',zipCode,t) #zipCode
        f.Texto_Mixto('xpath','//*[@id="customer.ssn"]',ssn,t) #ssn
        f.Texto_Mixto('xpath','//*[@id="customer.username"]',username,t) #username
        f.Texto_Mixto('xpath','//*[@id="customer.password"]',password,t) #password
        f.Texto_Mixto('id','repeatedPassword',repeatedPassword,t) #repeatedPassword
        f.Click_Mixto('xpath','//*[@id="customerForm"]/table/tbody/tr[13]/td[2]/input',t) #click en Register
        e1=f.SEXP('//*[@id="customer.username.errors"]').text
        if (e1==menssage):
            print('Prueba de Usuario ya resgitrado Exitosa |No permite acceso|')
        else:
            print('Prueba de Login Incorrecta |Permite acceso')
    
        #self.driver.close()
        
        

# Scenario 004 |Login|->Formato Incorrecto User y Paswoord
# Given que quiero probar login
# When  ingreso al sitio con User y Pasword Vacios
# Then  deberia informa que los datos son vacios            
######Secenario 003 Loguin###### 

    def L4(self,user,clave,menssage,t=2): #Formato User y Pasword Vacios
        print('###TEST -> User y Pasword Vacios###')    
        f=FuncionesGlobales(self.driver)
        f.Navegar('https://parabank.parasoft.com',t) #entra al navegador
        self.driver.maximize_window() #maximiza
        f.Texto_Mixto('xpath','//*[@id="loginPanel"]/form/div[1]/input',user,t) #selecciona user
        f.Texto_Mixto('xpath','//*[@id="loginPanel"]/form/div[2]/input',clave,t) #selecciona clave
        f.Click_Mixto('xpath','//*[@id="loginPanel"]/form/div[3]/input',t) #hace click en login
        e1=f.SEXP('//*[@id="rightPanel"]/p').text
        print('El error por enviar user vacion es ' + e1)
        if (e1==menssage):
            print('Prueba de Validacion con campo User, Clave o ambos vacio fue Exitosa |No permite acceso|')
        else:
            print('Prueba de Validacion con campo User, Clavem o ambos vacio fue Incorrecta |Permite acceso|')
        
        #self.driver.close()
    
# Scenario 005 |Login|->Formato Incorrecto User y Paswoord
# Given que quiero probar login
# When  ingreso al sitio con User y Pasword Vacios
# Then  deberia informa que los datos son vacios            
######Secenario 003 Loguin######     
        
    def L5(self,user,clave,menssage,t=2): #Clave vacio
        print('###TEST -> User o Pasword Incorrecto###')    
        f=FuncionesGlobales(self.driver)
        f.Navegar('https://parabank.parasoft.com',t) #entra al navegador
        self.driver.maximize_window() #maximiza
        f.Texto_Mixto('xpath','//*[@id="loginPanel"]/form/div[1]/input',user,t) #selecciona user
        f.Texto_Mixto('xpath','//*[@id="loginPanel"]/form/div[2]/input',clave,t) #selecciona clave
        f.Click_Mixto('xpath','//*[@id="loginPanel"]/form/div[3]/input',t) #hace click en login
        e1=f.SEXP('//*[@id="rightPanel"]/p').text
        print('El error por enviar user vacion es ' + e1)
        if (e1==menssage):
            print('Prueba de Validacion con campo User o Clave Incorrectos fue Exitosa |No permite acceso|')
        else:
            print('Prueba de Validacion con campo User o Clave Incorrectos fue Incorrecta |Permite acceso|')
        
        #self.driver.close()
     
     
    def L6(self,user,clave,menssage,t=2):  #Ingreso OK
        print('###TEST 5 -> Ingreso OK###')    
        f=FuncionesGlobales(self.driver)
        f.Navegar('https://parabank.parasoft.com',t) #entra al navegador
        self.driver.maximize_window() #maximiza
        f.Texto_Mixto('xpath','//*[@id="loginPanel"]/form/div[1]/input',user,t) #selecciona user
        f.Texto_Mixto('xpath','//*[@id="loginPanel"]/form/div[2]/input',clave,t) #selecciona clave
        f.Click_Mixto('xpath','//*[@id="loginPanel"]/form/div[3]/input',t) #hace click en login
        e1=f.SEXP('//*[@id="rightPanel"]/div/div/h1').text
        print(e1)
        if (e1==menssage):
            print('Login Exitoso')
        else:
            print('Login Incorrecto')
        


  