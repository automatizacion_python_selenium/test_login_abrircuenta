from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common import TimeoutException
from selenium.webdriver.support.ui import Select
from selenium.webdriver import ActionChains
from Funciones_Base import FuncionesGlobales
from Funciones_Registro_Login import Funciones_Login
from allure_commons.types import AttachmentType



import pytest
import allure

@pytest.fixture()
def log_on_failure(request):
  driver=webdriver.Chrome(ChromeDriverManager().install())  
  yield
  item = request.node
  if item.rep_call.failed:
    allure.attach(driver.get_screenshot_as_png(), name="Error", attachment_type=AttachmentType.PNG)

    
# Scenario 001 |Registo| ->Campos vacios
# Given que quiero probar el registo 
# When  realizo registro con capos requeridos vacios
# Then  deberia informarme que campos son requeridos sin permitir acceso
@pytest.mark.usefixtures('log_on_failure')
@pytest.mark.run
def test_login1(): 
  driver=webdriver.Chrome(ChromeDriverManager().install())  
  fl=Funciones_Login(driver)
  fl.L1('First name is required.','Last name is required.','Address is required.','City is required.','State is required.','Zip Code is required.','Social Security Number is required.','Username is required.','Password is required.','Password confirmation is required.')
  allure.attach(driver.get_screenshot_as_png(), name='TestCamposVacios', attachment_type=AttachmentType.PNG)
  driver.quit()


# Scenario 002 |Registo| ->Campos completos
# Given que quiero probar el registo 
# When  realizo registro con capos completos correctamente
# Then  permitir el registro ok
@pytest.mark.usefixtures('log_on_failure')
@pytest.mark.run
def test_login2(): 
  driver=webdriver.Chrome(ChromeDriverManager().install())  
  fl=Funciones_Login(driver)
  fl.L2('Maria','Rocca','Lima','Mendonza','Argentina','CP6000',"123456789",'@maria','Amaria123','Amaria123','Your account was created successfully. You are now logged in.')
  allure.attach(driver.get_screenshot_as_png(), name='Campos completos', attachment_type=AttachmentType.PNG)
  driver.quit()
 
# Scenario 003 |Registo| ->Con usuario vigente
# Given que quiero probar el registo 
# When  realizo registro con usario ya registrado
# Then  deberia no perimitrime el ingreso 
@pytest.mark.usefixtures('log_on_failure')
@pytest.mark.run
def test_login3(): 
  driver=webdriver.Chrome(ChromeDriverManager().install())  
  fl=Funciones_Login(driver)
  fl.L3('Maria','Rocca','Lima','Mendonza','Argentina','CP6000',"123456789",'@maria','Amaria123','Amaria123','This username already exists.')
  allure.attach(driver.get_screenshot_as_png(), name='Usuario vigente', attachment_type=AttachmentType.PNG)
  driver.quit()

 
# Scenario 003 |Login|->Formato User vacio
# Given que quiero probar login
# When  ingreso al sitio con User vacio
# Then  deberia informa que los datos son incorrectos
@pytest.mark.usefixtures('log_on_failure')
@pytest.mark.run
def test_login4(): 
  driver=webdriver.Chrome(ChromeDriverManager().install())  
  fl=Funciones_Login(driver)
  fl.L4('', 'Amaria123','Please enter a username and password.')
  allure.attach(driver.get_screenshot_as_png(), name='Formato User vacio', attachment_type=AttachmentType.PNG)
  driver.quit()

# Scenario 003 |Login|->Formato Password vacio
# Given que quiero probar login
# When  ingreso al sitio con Password vacio
# Then  deberia informa que los datos son incorrectos
@pytest.mark.usefixtures('log_on_failure')
@pytest.mark.run
def test_login5():
  driver=webdriver.Chrome(ChromeDriverManager().install())
  fl=Funciones_Login(driver)
  fl.L4('Amaria','','Please enter a username and password.')
  allure.attach(driver.get_screenshot_as_png(), name='Formato Password vacio', attachment_type=AttachmentType.PNG)
  driver.quit()

# Scenario 003 |Login|->Formato Password y User vacios
# Given que quiero probar login
# When  ingreso al sitio con Password vacio
# Then  deberia informa que los datos son incorrectos
@pytest.mark.usefixtures('log_on_failure')
@pytest.mark.run
def test_login6():
  driver=webdriver.Chrome(ChromeDriverManager().install())
  fl=Funciones_Login(driver)
  fl.L4('','','Please enter a username and password.')
  allure.attach(driver.get_screenshot_as_png(), name='Formato Password y User vacios', attachment_type=AttachmentType.PNG)
  driver.close()

# Scenario 004 |Login|->Formato Incorrecto User y Paswoord
# Given que quiero probar login
# When  ingreso al sitio con User y Pasword Vacios
# Then  deberia informa que los datos son vacios            
######Secenario 003 Loguin######     
@pytest.mark.usefixtures('log_on_failure')
@pytest.mark.run
def test_login7():
  driver=webdriver.Chrome(ChromeDriverManager().install())
  fl=Funciones_Login(driver)
  fl.L5('1234','Amaria123','The username and password could not be verified.')
  allure.attach(driver.get_screenshot_as_png(), name='Formato Incorrecto User y Paswoord', attachment_type=AttachmentType.PNG)
  driver.quit()

@pytest.mark.usefixtures('log_on_failure')
@pytest.mark.run
def test_login8():
  driver=webdriver.Chrome(ChromeDriverManager().install())
  fl=Funciones_Login(driver)
  fl.L5('Amaria','1234','The username and password could not be verified.')
  allure.attach(driver.get_screenshot_as_png(), name='Formato Incorrecto User y Paswoord', attachment_type=AttachmentType.PNG)
  driver.quit()

# Scenario 005 |Login|->Ingreso OK
# Given que quiero probar login 
# When  ingreso al sitio con los datos correctos
# Then  deberia informa que realice login ok
@pytest.mark.usefixtures('log_on_failure')
@pytest.mark.run
def test_login9():
  driver=webdriver.Chrome(ChromeDriverManager().install())
  fl=Funciones_Login(driver)
  fl.L6('@maria','Amaria123','Accounts Overview')
  allure.attach(driver.get_screenshot_as_png(), name='Ingreso OK', attachment_type=AttachmentType.PNG)
  driver.quit()


