from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common import TimeoutException
from selenium.webdriver.support.ui import Select 
from selenium.webdriver import ActionChains

from Funciones_Base import FuncionesGlobales
from Funciones_Registro_Login import Funciones_Login

import pytest
import time

driver = ' '
class Funciones_Cuenta():
    

######INICIALIZAR######
    def __init__(self,driver): #self mi o mio
        self.driver=driver
        
# Scenario 001 |Registo| ->Campos vacios
# Given que quiero probar el registo 
# When  realizo registro con capos requeridos vacios
# Then  deberia informarme que campos son requeridos sin permitir acceso

    def N1(self,menssage,t=2): #Campos vacios requeridos
        print('###TEST  -> Creacion de Cuenta Nueva###' )    
        f=FuncionesGlobales(self.driver)
        fl=Funciones_Login(self.driver)
        fl.L6('@maria','Amaria123','Accounts Overview')
        f.Click_Mixto('xpath','//*[@id="leftPanel"]/ul/li[1]/a',t) #hace click en registro
        f.Select_ID_Type('type','value','1',t) #Selecciona el Nuevo tiempo de cuenta
        f.Select_ID_Type('fromAccountId','value','16896',t) #Selecciona cuenta de donde va a realizar tranferencia
        f.Click_Mixto('xpath','//*[@id="rightPanel"]/div/div/form/div/input',2) #Hace click en boton OPEN
        m1=f.SEXP('//*[@id="rightPanel"]/div/div/h1').text 
        if (m1==menssage):
            print('Prueba de Validacion Creacion de Cuenta OK |CUENTA CREADA|')
        else:
            print('Prueba de Validacion Creacion de Cuenta OK |CUENTA NO CREADA|')
        #self.driver.quit() 

