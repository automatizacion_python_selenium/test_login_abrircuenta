from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common import TimeoutException
from selenium.webdriver.support.ui import Select
from selenium.webdriver import ActionChains
from allure_commons.types import AttachmentType

from Funciones_Base import FuncionesGlobales
from Funciones_Abri_NuevaCuenta import Funciones_Cuenta

import pytest
import allure

# Scenario 001 |CuentaNueva| ->Creacion ok
# Given que quiero probar la creacion de una cuenta  
# When  selecciono tipo de cuenta y cuenta de donde se realiza tranferencia
# Then  deberia informarme cuenta creada
@pytest.mark.run
def test_login1(): 
  driver=webdriver.Chrome(ChromeDriverManager().install())  
  fn=Funciones_Cuenta(driver)
  fn.N1('Account Opened!',2)
  allure.attach(driver.get_screenshot_as_png(), name='Creacion Cuenta Nueva ok', attachment_type=AttachmentType.PNG)
  driver.quit()