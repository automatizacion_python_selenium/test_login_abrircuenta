# ‣ Lenguaje de Programacion 
• Python

# ‣ Entorno de pruebas de software 
• Selenium

# ‣ Requerimiento
• Python<br>
• Selenium<br>
• PyTest<br>
• Allure<br>

# ‣ Ejecucion del Proyecto
• Ejecutar Test Test_Registro_Login.py por terminal: <b>pytest Test_Registro_Login.py -s -v</b><br>
• Ejecutar Test Test_Abrir_NuevaCuenta.py por terminal: <b>pytest Test_Abrir_NuevaCuenta -s -v -m run</b><br>
• Ejecturar Serve por terminal: <b>allure serve Reportes </b><br>



