from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common import TimeoutException
from selenium.webdriver.support.ui import Select 
from selenium.webdriver import ActionChains

import unittest
import time

driver = ' ' #lo dejo vacio para que cada vez que lo llamen a funciones base trae el driver que tiene en el init

class FuncionesGlobales():
    
######INICIALIZAR######
    def __init__(self,driver):
        self.driver=driver
    
    ######FUNCION SALUDO, PARA VER IMPRIMIR (no la uso)######
    def Saludos(self):
        print('Bienvenido a Page Objet Model')
    
    
######Tiempo######
    def Tiempo(self,tiempo):
        t=time.sleep(tiempo)
        return t
    
    
######Navegador######
    def Navegar(self,url,tiempo):
        self.driver.get(url)
        self.driver.maximize_window()
        print("Pagina abierta: "+ str(url))
        t=time.sleep(tiempo)
        return t

           
#Funcion BASE que encuentra elemento por ID o Xpath,
    def SEXP(self,elemento):
        val=WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.XPATH,elemento)))
        val=self.driver.execute_script('arguments[0].scrollIntoView();',val)
        val=self.driver.find_element(By.XPATH,elemento)
        return val
    
    def SEID(self,elemento):
        val=WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.ID,elemento)))
        val=self.driver.execute_script('arguments[0].scrollIntoView();',val)
        val=self.driver.find_element(By.ID,elemento)
        return val
        
        
    #Funcion MULTIPLE que encuentra elemento por ID o Xpath,
    #hace scroll hasta encontrarlo e ingresa texto   
    def Texto_Mixto(self,tipo,selector,texto,tiempo):
        if(tipo=='xpath'):
            try:
                val=self.SEXP(selector)
                val.clear()
                val.send_keys(texto)
                print(f'Escribiendo en el campo {selector} el texto {texto}')
                t=time.sleep(tiempo)
                return t
            except TimeoutException as ex:
                print(ex.msg)
                print('No se encontro el elemento '+ selector)
        elif(tipo=='id'):
            try:
                val=self.SEID(selector)
                val.clear()
                val.send_keys(texto)
                print(f'Escribiendo en el campo {selector} el texto {texto}')
                t=time.sleep(tiempo)
                return t
            except TimeoutException as ex:
                print(ex.msg)
                print('No se encontro el elemento '+ selector)
                
              
    #Funcion MULTIPLE que hace click elemento por ID o Xpath haciendo scroll
    def Click_Mixto(self,tipo,selector,tiempo):
        if(tipo=='xpath'):
            try:
                val=self.SEXP(selector)
                val.click()
                print(f'Damos Click en el campo {selector}')
                t=time.sleep(tiempo)
                return t
            except TimeoutException as ex:
                print(ex.msg)
                print('No se encontro el elemento '+ selector)
        elif(tipo=='id'):
            try:
                val=self.SEID(selector)
                val.click()
                print(f'Damos Click en el campo {selector}')
                t=time.sleep(tiempo)
                return t
            except TimeoutException as ex:
                print(ex.msg)
                print('No se encontro el elemento '+ selector)
                
     
    #Funcion SELEC por ID por Text, Value e Index,haciendo srcoll 
    def Select_ID_Type(self,id,tipo,dato,tiempo):
        try:
            val=self.SEID(id)
            val=Select(self.driver.find_element(By.ID,id))
            if (tipo=='text'):
                val.select_by_visible_text(dato)
            elif(tipo=='value'):
                val.select_by_value(dato)
            elif(tipo=='index'):
                val.select_by_index(dato) 
            print(f'El campo seleccionado es {dato}')
            t=time.sleep(tiempo)
            return t
        except TimeoutException as ex:
            print(ex.msg)
            print('No se encontro el elemento '+ id)
    
    
   
            
    
